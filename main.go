package main

import (
	"flag"
	"fmt"
	"os"
	"os/exec"
	"strings"
	"sync"
)

var wg sync.WaitGroup

func CheckError(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: %s\n", err)
	}
}

func runCommand(command []string) {
	defer wg.Done()
	cmd := exec.Command(command[0], command[1:]...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Start()
	CheckError(err)
	err = cmd.Wait()
	CheckError(err)
}

func executeCommands(variables []string, commandArgs []string) {
	for _, variable := range variables {
		command := []string{}
		for _, arg := range commandArgs {
			command = append(command, strings.Replace(arg, "%", variable, -1))
		}
		wg.Add(1)
		go runCommand(command)
	}
}

func main() {
	// Define flags
	nFlag := flag.Int("n", 0, "Number of jobs to run (if set, overrides -a)")
	aFlag := flag.String("a", "", "States to run (comma-separated)")
	sFlag := flag.Int("s", 0, "Starting number for iterations")
	lFlag := flag.Int("l", 0, "Number of leading zeros to use")
	flag.Parse()

	// Check for conflicting flags
	if *nFlag > 0 && *aFlag != "" {
		fmt.Fprintln(os.Stderr, "Error: Cannot define both -n and -a at the same time.")
		os.Exit(1)
	} else if *nFlag == 0 && *aFlag == "" {
		fmt.Fprintf(os.Stderr, "Usage: %s -n <number> | -a state1,state2,... -s <start>\nExample: %s -n 11 | %s -a arg1,arg2 -s 3\n", os.Args[0], os.Args[0], os.Args[0])
		os.Exit(1)
	}

	// Determine number of leading zeros
	leadingZeros := len(fmt.Sprintf("%d", *nFlag))
	if *lFlag > leadingZeros {
		leadingZeros = *lFlag
	}

	// Determine whether to use the number of jobs or the states
	if *nFlag > 0 {
		fmt.Printf("running for %d jobs\n", *nFlag)
		var variables []string
		start := *sFlag
		for i := start; i < start+*nFlag; i++ {
			variable := fmt.Sprintf("%0*d", leadingZeros, i) // Format with leading zeros
			variables = append(variables, variable)
		}
		executeCommands(variables, flag.Args())
	} else {
		fmt.Printf("running for states: %q\n", strings.Split(*aFlag, ","))
		executeCommands(strings.Split(*aFlag, ","), flag.Args())
	}
	wg.Wait()
}
